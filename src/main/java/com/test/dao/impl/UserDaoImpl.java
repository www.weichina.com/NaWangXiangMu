package com.test.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.test.dao.UserDao;
import com.test.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by wei on 2017/10/30.
 */
@Repository
public class UserDaoImpl  implements UserDao {
   @Autowired
   private HibernateTemplate hibernateTemplate;

    public String login() {
        List<User> users = hibernateTemplate.loadAll(User.class);
        return JSONObject.toJSONString(users);
    }
}
